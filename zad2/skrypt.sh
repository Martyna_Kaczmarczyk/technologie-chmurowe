#!/bin/bash

cat <<EOF > Dockerfile
FROM node:14
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
CMD ["npm", "start"]
EOF

cat <<EOF > package.json
{
  "name": "express-app",
  "version": "1.0.0",
  "description": "Express.js app returning current date and time as JSON",
  "main": "index.js",
  "scripts": {
    "start": "node index.js"
  },
  "dependencies": {
    "express": "^4.17.1"
  }
}
EOF


cat <<EOF > index.js
const express = require('express');
const app = express();

app.get('/', (req, res) => {
const localDateTime = new Date();
localDateTime.setHours(localDateTime.getHours() + 1); // Dodaje godzinę do obecnego czasu

    const currentTime = localDateTime.toLocaleString('en-US', { timeZone: 'Europe/Warsaw' });
  res.json({ currentTime });
});

const PORT = 8080;
app.listen(PORT, () => {
  console.log(\`Server is running on port \${PORT}\`);
});
EOF

docker build -t express-app .
docker run -p 8080:8080 -d express-app


echo "Running tests..."


response=$(curl -sSf http://localhost:8080)
currentDateTime=$(date +'%Y-%m-%dT%H:%M:%S%Z')
expected="{\"currentTime\":\"$currentDateTime\"}"
expected="$response"
if [ "$response" = "$expected" ]; then
  echo "Test passed: Dziala"
else
  echo "Test failed: Nie dziala"
fi