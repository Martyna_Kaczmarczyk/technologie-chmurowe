#!/bin/bash

cat <<EOF > Dockerfile
FROM node:12
WORKDIR /app
COPY . .
CMD ["bash", "-c", "node server.js"]
EOF

cat <<EOF > server.js
const http = require('http');

const hostname = '0.0.0.0';
const port = 8080;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

server.listen(port, hostname, () => {
  console.log(\`Server running at http://\${hostname}:\${port}/\`);
});
EOF


docker build -t nodejs-server .
docker run -p 8080:8080 -d nodejs-server

# Testy
echo "Running tests..."
sleep 2 

response=$(curl -sSf http://localhost:8080)
expected="Hello World"

if [ "$response" = "$expected" ]; then
  echo "Test passed: Dziala"
else
  echo "Test failed: Nie dziala"
fi
