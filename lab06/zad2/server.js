// const express = require('express');
// const mysql = require('mysql2');

// const app = express();
// const client = mysql.createConnection({
//   host: 'mysql-server',
//   port: 6379,
// });
// client.set('visits', 0);

// app.get('/', (req, res) => {
//     client.query('SELECT count FROM *', (err, rows) => {
//         if (err) {
//           console.error('Error reading data from MySQL:', err);
//           return;
//         }
//         res.send('Database connected');
//       });
// });

// app.listen(8081, () => {
//   console.log('listening on port 8081');
// });

// const connection = mysql.createConnection({
//   host: 'mysql-server',
//   port: 3306, // MySQL default port
//   user: 'root', // MySQL default user
//   password: 'password', // Change this to your MySQL password
// });

// connection.connect((err) => {
//   if (err) {
//     console.error('Error connecting to MySQL:', err);
//     return;
//   }
//   console.log('Connected to MySQL');
// });

// app.get('/', (req, res) => {
//   connection.query('SELECT count(*) as count FROM your_table_name', (err, rows) => {
//     if (err) {
//       console.error('Error reading data from MySQL:', err);
//       return;
//     }
//     res.send(`Number of visits: ${rows[0].count}`);
//   });
// });

// app.listen(8081, () => {
//   console.log('Listening on port 8081');
// });

// const express = require("express");
// const { MongoClient } = require("mongodb");

// const app = express();
// const port = 8080;

// // const url = "mongodb://localhost:27017/test_example"
// const url = "mongodb://mongodb:27017"; 
// const dbName = "test";

// app.get("/", async (req, res) => {
//   try {
//     const client = new MongoClient(url);
//     await client.connect();
//     const db = client.db(dbName);
//     const collection = db.collection("restaurants");
//     const result = await collection.find({}).toArray();
//     res.json(result);
//     client.close();
//   } catch (err) {
//     console.error(err);
//     res.status(500).send("Internal Server Error");
//   }
// });

// app.listen(port, () => {
//   console.log(`App listening at http://localhost:${port}`);
// });

const express = require("express");
const { MongoClient } = require("mongodb");

const app = express();
const port = 8080;

const url = "mongodb://host.docker.internal:27017"
const dbName = "test";
app.get("/", async (req, res) => {
  try {
    const client = new MongoClient(url);
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection("restaurants");
    const result = await collection.find({}).toArray();
    res.json(result);
    client.close();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
