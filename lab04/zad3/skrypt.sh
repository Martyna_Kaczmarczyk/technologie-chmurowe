# LINUX

# #!/bin/bash

# # Pobranie listy woluminów Docker
# volumes=$(docker volume ls --format "{{.Name}}")

# # Iteracja przez każdy wolumin i wyświetlenie jego zużycia przestrzeni dyskowej w procentach
# for volume in $volumes
# do
#     echo "Volume: $volume"
    
#     # Pobranie informacji o zużyciu przestrzeni dyskowej dla danego woluminu
#     usage=$(docker run --rm -v $volume:/volume busybox df -h /volume | awk 'NR==2 {print $5}')
    
#     echo "Disk usage: $usage"
# done


#WINDOWS    

#!/bin/bash

volumes=$(docker volume ls --format "{{.Name}}")

for volume in $volumes
do
    echo "Volume: $volume"
    usage=$(docker run --rm -v $volume:/volume busybox df -h | grep "/volume$" | tr -s ' ' | cut -d' ' -f5)    
    echo "Disk usage: $usage"
done
