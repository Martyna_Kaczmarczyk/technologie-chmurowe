#!/bin/bash

docker volume create nodejs_data
docker volume create all_volumes
docker run --rm -v nginx_volume:/source -v all_volumes:/destination busybox sh -c "cp -R /source/. /destination"
docker run --rm -it --name nodejs_container -v nodejs_data:/app -v all_volumes:/destination node:latest sh -c "touch /app/test.txt && cp -R /app/. /destination"

