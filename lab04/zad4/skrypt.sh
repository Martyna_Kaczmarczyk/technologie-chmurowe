#!/bin/bash

VOLUME_NAME="all_volumes"
TEMP_FILE="/backup/encrypted_volume.tar.gz"
haslo="haslo"

docker run --rm -v $VOLUME_NAME:/data -v ${PWD}:/backup alpine tar czf /backup/encrypted_volume.tar.gz
gpg --output $TEMP_FILE.gpg --symmetric --cipher-algo $haslo $TEMP_FILE
rm $TEMP_FILE

# Odszyfrowanie pliku archiwum
gpg --output $TEMP_FILE --decrypt $TEMP_FILE.gpg

# Rozpakowanie archiwum
tar xzf $TEMP_FILE -C .

# Usunięcie zaszyfrowanego pliku tymczasowego
rm $TEMP_FILE $TEMP_FILE.gpg