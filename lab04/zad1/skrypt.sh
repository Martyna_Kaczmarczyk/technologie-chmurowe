#!/bin/bash


docker volume create nginx_volume
docker run -d -p 80:80 --name nagix_with_volume --volume nginx_volume:/usr/share/nginx/html nginx
echo "<html><body><h1>Hello from Nginx!</h1></body></html>" | docker exec -i nagix_with_volume sh -c 'cat > /usr/share/nginx/html/index.html'