#!/bin/bash

# Uruchomienie kontenera MongoDB
docker run -d --name mongo-test mongo

# Pobranie adresu IP hosta dla systemów Linux
if [[ "$(uname)" == "Linux" ]]; then
    host_ip=$(ip route | awk 'NR==1 {print $3}')
else
    host_ip="host.docker.internal"
fi

# Uruchomienie kontenera Node.js z aplikacją Express.js i połączeniem z bazą MongoDB
docker run -d -p 8080:8080 \
        --name node-app \
        -e MONGODB_URI="mongodb://${host_ip}:27017/test" \
        node:16 \
    bash -c "npm install express mongoose && node -e 'const express = require(\"express\"); const mongoose = require(\"mongoose\"); 
    const app = express(); mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true }); 
    app.get(\"/\", (req, res) => { res.send(\"Hello from Express.js!\"); }); 
        app.listen(8080, () => { console.log(\"Server is running on port 8080\"); 
    });'"

# Testy
# Sprawdzenie czy kontenery są uruchomione
if docker ps -a --format '{{.Names}}' | grep -q 'mongo-test' && docker ps -a --format '{{.Names}}' | grep -q 'node-app'; then
    echo "Kontenery zostały poprawnie uruchomione."
else
    echo "Błąd: Nie udało się uruchomić kontenerów."
fi

# Sprawdzenie czy aplikacja zwraca dane z bazy MongoDB
response=$(curl -s http://localhost:8080)
if [[ "$response" == "Hello from Express.js!" ]]; then
    echo "Aplikacja działa poprawnie i zwraca dane z bazy MongoDB."
else
    echo "Błąd: Aplikacja nie zwraca poprawnych danych z bazy MongoDB."
fi
