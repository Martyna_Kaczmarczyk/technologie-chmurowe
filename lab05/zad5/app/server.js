const express = require("express");
const fs = require("fs");
const path = require("path");

const app = express();
const port = 3000;

// Ścieżka do pliku index.html
const indexPath = path.join(__dirname, "index.html");

// Obsługa żądania do pliku index.html
app.get("/", (req, res) => {
    // Odczytaj zawartość pliku index.html
res.send("Hiiii")
});

// Nasłuch na określonym porcie
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});