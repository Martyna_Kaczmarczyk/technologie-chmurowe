#!/bin/bash

config_file="default.conf"
port=3090

show_help() {
    echo "Użycie: $0 [-c <config_file>] [-p <port>]"
    echo "Tworzy kontener Docker z serwerem Nginx i umożliwia dodanie niestandardowej konfiguracji."
    echo "Opcje:"
    echo "  -c <config_file>  Ścieżka do niestandardowego pliku konfiguracyjnego Nginx (domyślnie: default.conf)"
    echo "  -p <port>         Port, na którym serwer Nginx będzie nasłuchiwał (domyślnie: 80)"
    exit 1
}

while getopts "c:p:" opt; do
    case $opt in
        c) config_file=$OPTARG ;;
        p) port=$OPTARG ;;
        *) show_help ;;
    esac
done

docker run -d -p $port:80 \
    -v "$(pwd)/$config_file":/$config_file:ro \
    --name nginx_container \
    nginx

echo "Kontener Nginx został utworzony i uruchomiony na porcie $port z konfiguracją $config_file."

docker ps | grep -q nginx_container
if [ $? -eq 0 ]; then
    echo "Test 1: Kontener Nginx został poprawnie utworzony - OK"
else
    echo "Test 1: Błąd! Kontener Nginx nie został utworzony."
fi

docker ps | grep nginx_container | grep -q "0.0.0.0:$port->80/tcp"
if [ $? -eq 0 ]; then
    echo "Test 3: Serwer Nginx nasłuchuje na porcie $port - OK"
else
    echo "Test 3: Błąd! Serwer Nginx nie nasłuchuje na porcie $port."
fi

response=$(curl -s -o /dev/null -w "%{http_code}" http://localhost:$port)
if [ $response -eq 200 ]; then
    echo "Test 4: Testowa strona jest dostępna - OK"
else
    echo "Test 4: Błąd! Testowa strona nie jest dostępna."
fi
