const express = require("express");
const { MongoClient } = require("mongodb");

const app = express();
const port = 8080;

// const url = "mongodb://localhost:27017/test_example"
const url = "mongodb://host.docker.internal:27017";
const dbName = "test";

app.get("/", async (req, res) => {
  try {
    const client = new MongoClient(url);
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection("restaurants");
    const result = await collection.find({}).toArray();
    res.json(result);
    client.close();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});