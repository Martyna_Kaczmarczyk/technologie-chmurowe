#!/bin/bash

docker build -t myapp .
docker run -d --name myapp-container -p 8080:8080 myapp

# Testy
sleep 5  # Czekaj, aż aplikacja będzie gotowa
response=$(curl -s localhost:8080)
if [[ $response == *"Internal Server Error"* ]]; then
  echo "Test failed: Internal Server Error"
  exit 1
else
  echo "Test passed: Application is running"
fi

# Sprzątanie - usuwanie kontenera po zakończeniu testów
docker rm -f myapp-container