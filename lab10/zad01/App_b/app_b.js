const express = require('express');
const app = express();
const port = 4000;

app.get('/api', (req, res) => {
    res.send('Hello from Mikroserwis B');
});

app.listen(port, () => {
    console.log(`Mikroserwis B listening at http://localhost:${port}`);
});
