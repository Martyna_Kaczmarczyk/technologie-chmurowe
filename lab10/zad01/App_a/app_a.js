const express = require('express');
const axios = require('axios');
const app = express();
const port = 3000;

app.get('/api', async (req, res) => {
    try {
        const response = await axios.get('http://mikroserwis-b-service:4000/api');
        res.send(response.data);
    } catch (error) {
        res.status(500).send('Error: ' + error.message);
    }
});

app.listen(port, () => {
    console.log(`Mikroserwis A listening at http://localhost:${port}`);
});
